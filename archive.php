<?php

/**
 * The main template file.
 */

get_header();

$taxonomy = get_queried_object();
$taxonomy_name = $taxonomy->name;
$taxonomy_url = get_term_link($taxonomy);
$not_in = get_field('featured_article', 'options') ? array(get_field('featured_article', 'options')->ID) : array();

?>

<main role="main">

<div class="section section--normal-padding">
      <div class="container">
        <div class="search-archive-bar">
          <div class="row row--25">
            <div class="col col--6 col--sm-12">
              <form action="" class="archive-filters-form archive-filters-form--video">
                <select name="filter-videos" class="search-archive-input font--tilt jsFormFilterInput" id="filter-videos">
                  <option value="<?php echo esc_url(get_term_link($taxonomy_url)); ?>"><?= $taxonomy_name ?></option>

                  <?php $terms = get_terms('article_type', array('hide_empty' => true));
                    foreach ( $terms as $term ): ?>
                      <?php if ( $term->name !== $taxonomy_name ): ?>
                        <option value="<?php echo esc_url(get_term_link($term)); ?>"><?php echo $term->name; ?></option>
                      <?php endif; ?>
                  <?php endforeach; ?>

                  <option value="<?php echo get_post_type_archive_link('post'); ?>">All Articles</option>
                </select>
              </form>
            </div>
            <!-- /.col6 -->
            <div class="col col--6 col--sm-12">
              <form action="<?php echo home_url('/'); ?>" method="GET" class="archive-search-form archive-search-form--video">
                <input type="hidden" name="post_type" value="post">
                <input type="search" class="search-archive-input font--tilt" value="<?php echo get_search_query(); ?>" name="s" placeholder="Search Articles">
              </form>
            </div>
            <!-- /.col6 -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.search-archive-bar -->
      </div>
    </div>



  <?php $tax_query = ''; if ( is_tax('article_type') ) {
    $tax_query = array(
      array(
        'taxonomy'    => 'article_type',
        'field'       => 'slug',
        'terms'       => get_query_var('term'),
        'operator'    => 'IN'
      )
    );
  } ?>

  <div class="section section--normal-padding">
    <div class="container">
      <div class="news-wrapper" data-tax="<?php echo get_query_var('term'); ?>" data-s="<?php echo get_search_query(); ?>">
        <?php $query = new WP_Query(array(
          'post_type'       => 'post',
          'posts_per_page'  => 5,
          's'               => get_search_query(),
	      'post__not_in'	=> $not_in,
          /*'meta_query'      => array(
            array(
              'key'         => 'featured_post',
              'compare'     => '!=',
              'value'       => 1,
            )
          ),*/
          'tax_query'       => $tax_query
        )); if ( $query->have_posts() ): $i = 0; while ( $query->have_posts() ): $query->the_post(); ?>
        <div class="item-wrapper<?php if ( $i == 4 ) echo ' item-wrapper--2col'; ?>">
          <div class="item<?php if ( $i == 4 ) echo ' item--2col'; ?>">
            <?php $terms = get_the_terms(get_the_ID(), 'article_type'); if ( $terms ): ?>
            <a href="<?php echo esc_url(get_term_link($terms[0])); ?>" class="term" style="background-color: <?php echo get_field('color', $terms[0]); ?>;"><?php echo $terms[0]->name; ?></a>
            <?php endif; ?>
            <a href="<?php echo esc_url(get_permalink()); ?>" class="thumbnail with-background-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>)"></a>
            <a href="<?php echo esc_url(get_permalink()); ?>" class="content">
              <h2 class="font--tilt"><?php the_title(); ?></h2>
              <div class="meta">
                <span class="date"><?php the_time('d F Y, g:iA'); ?></span>
                <span class="comments"><i class="fa fa-comment"></i><?php echo get_comments_number(); ?> <?php _e('Comments', 'psdtheme'); ?></span>
              </div>
              <p><?php echo wp_trim_words(get_the_excerpt(), 13); ?></p>
            </a>
          </div>
        </div>
        <?php $i++; endwhile; wp_reset_postdata(); endif; ?>
        <?php get_template_part('template-parts/ads/articles-1'); ?>
      </div><!-- ./news-wrapper -->
      <?php $query_all = new WP_Query(array(
        'post_type'       => 'post',
        'posts_per_page'  => -1,
        's'               => get_search_query(),
		'post__not_in'	  => $not_in,
        /*'meta_query'      => array(
          array(
            'key'         => 'featured_post',
            'compare'     => '!=',
            'value'       => 1,
          )
        ),*/
        'tax_query'       => $tax_query
      )); if ( $query_all->have_posts() && $query_all->post_count > $query->post_count ): ?>
      <a href="#" class="jsNewsAjax btn btn--ucase btn--section"><?php _e('Load More Articles', 'psdtheme'); ?></a>
      <?php endif; ?>
    </div>
  </div>

</main>

<?php get_footer(); ?>
