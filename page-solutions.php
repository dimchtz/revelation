<?php

/**
 * Template Name: Solutions
 */

get_header(); ?>

<main role="main" class="main-content">

  <div class="solutions__nav">
    <a href="#">Reveal.</a>
    <a href="#">Resonate.</a>
    <a href="#">Relate.</a>
    <a href="#">Rejoice.</a>
  </div><!-- ./solutions__nav -->

  <div class="hero-solutions">
    <div class="container">
      <div class="hero-text">
        <div class="hero-text__left">
          <h1>How we do digital</h1>
          <h2>There's lots of complex marketing problems.</h2>
        </div>
        <div class="hero-text__right">
          <h3>We offer 4 simple solutions</h3>
          <p>See how RD' R's can add massive value to your digital advertising efforts... and even help marketing teams rest easier at night!</p>
        </div>
      </div><!-- ./hero-text -->
      <hr class="hero-sep--vertical">
    </div>
  </div><!-- ./hero-solutions -->

  <div class="home-solutions-part home-solutions-part-alt">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-1.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>EVEAL<span class="purple-text">.</span></h2>
        <p>Our real-time REVEAL product helps revolutionise the way teams visualise and analyse their business dashboards & data.</p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-solutions-part home-solutions-part-alt">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-2.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>ESONATE<span class="purple-text">.</span></h2>
        <p>See how our Creative Team designs & develops more engaging brand assets to better RESONATE with more audiences, across more formats.</p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-solutions-part home-solutions-part-alt">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-3.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>ELATE<span class="purple-text">.</span></h2>
        <p>Discover digital advertising that can really RELATE to your customers using Real-Time Bidding and Programmatic Media Buying across all channels.</p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-solutions-part home-solutions-part-alt home-solutions-part-last">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-4.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>EJOICE<span class="purple-text">.</span></h2>
        <p>You can only truly REJOICE once a campaign delivers a HUGE return on investment.</p>
        <p>So warm up those vocal cords and give us an R… O… I!? </p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->

  <div class="home-testimonials">
    <div class="line-vertical-center line-vertical-center-top-over"></div>
    <div class="diagonal-line"></div>
    <div class="container">
      <div class="text">
        <h2>SOLVING <span>THE PUZZLE.</span></h2>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
    <div class="testimonials-slider">
      <div class="slide">
        <div class="container">
          <div class="img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" class="pull-right" alt="">
          </div>
          <!-- /.img -->
          <div class="text">
            <h4>Client Name</h4>
            <p>Our job day in & day out is to help uncover exciting revelations about our client’s businesses, their audiences and where/how they should be better investing in media to boost business growth.</p>
            <a href="#" class="read-more">SEE THE STORY</a>
          </div>
          <!-- /.text -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.slide -->
      <div class="slide">
        <div class="container">
          <div class="img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" class="pull-right" alt="">
          </div>
          <!-- /.img -->
          <div class="text">
            <h4>Client Name</h4>
            <p>Our job day in & day out is to help uncover exciting revelations about our client’s businesses, their audiences and where/how they should be better investing in media to boost business growth.</p>
            <a href="#" class="read-more">SEE THE STORY</a>
          </div>
          <!-- /.text -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.slide -->
      <div class="slide">
        <div class="container">
          <div class="img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" class="pull-right" alt="">
          </div>
          <!-- /.img -->
          <div class="text">
            <h4>Client Name</h4>
            <p>Our job day in & day out is to help uncover exciting revelations about our client’s businesses, their audiences and where/how they should be better investing in media to boost business growth.</p>
            <a href="#" class="read-more">SEE THE STORY</a>
          </div>
          <!-- /.text -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.slide -->
    </div>
    <!-- /.testimonials-slider -->
  </div>
  <!-- /.home-testimonials -->
  <div class="get-in-touch">
  <div class="line-vertical-center line-vertical-center-top-alt"></div>
    <div class="container container-980">
      <div class="row row--80">
        <div class="col col--6 col--sm-12">
          <h2>GET IN <span>TOUCH</span></h2>
        </div>
        <!-- /.col col--6 -->
        <div class="col col--6 col--sm-12">
          <p>Does your Marketing need a Revelation? Pop us a call, leave a note or come visit us in person! </p>
          <a href="#" class="btn btn-white">SAY HELLO</a>
        </div>
        <!-- /.col col--6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.get-in-touch -->

</main>

<?php get_footer(); ?>
