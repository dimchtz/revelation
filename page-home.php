<?php

/**
 * Template Name: Home
 */

get_header(); ?>

<main role="main" class="main-content">
  <div class="hero-home" style="background-image: url(<?= get_field('hero_image'); ?>"></div>
  <div class="hero-home-title">
    <div class="text">
      <h1><?= get_field('hero_title'); ?></h1>
      <p><?= get_field('hero_second_text'); ?></p>
    </div>
    <div class="line-vertical-center"></div>
  </div>
  <!-- /.hero-home-title -->
  <div class="home-text-1">
    <div class="container">
      <?= get_field('intro_section_heading'); ?>
      <div class="row row--100">
        <div class="col col--7 col--sm-12">
        <p><?= get_field('intro_section_paragraph'); ?></p>
        </div>
        <!-- /.col col--7 col--sm-12 -->
        <div class="col col--5 col--sm-12">
          <?php $learn_more_link = get_field('intro_section_learn_more_link'); ?>

          <?php if( $learn_more_link ): ?>
            <a href="<?= $learn_more_link['url']; ?>"<?= $learn_more_link['target'] ? 'target='.$learn_more_link['target'] : ''; ?> class="btn btn-purple"><?= $learn_more_link['title']; ?></a>
          <?php endif; ?>
        </div>
        <!-- /.col col--5 col--sm-12 -->
      </div>
      <!-- /.row -->
      <h4 class="hide--sm-down"><?= get_field('intro_section_welcome_text'); ?></h4>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-text-1 -->
  <div class="home-solutions-intro">
    <div class="line-vertical-center line-vertical-center-top"></div>
    <div class="container">
      <div class="row row--65">
        <div class="col col--7 col--sm-12 pull-right">
          <h2>OUR<br/>SOLUTIONS</h2>
        </div>
        <!-- /.col col--7 col--sm-12 -->
        <div class="col col--5 col--sm-12">
          <p>There are so many complex marketing problems.
            We offer 4 Simple Solutions <br/>- <span class="purple-text">the 4R's.</span></p>
        </div>
        <!-- /.col col--5 col--sm-12 -->
      </div>
      <!-- /.row row--65 -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-intro -->
  <div class="home-solutions-part">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-1.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>EVEAL<span class="purple-text">.</span></h2>
        <p>Our real-time REVEAL product helps revolutionise the way teams visualise and analyse their business dashboards & data.</p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-solutions-part home-solutions-part-alt">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-2.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>ESONATE<span class="purple-text">.</span></h2>
        <p>See how our Creative Team designs & develops more engaging brand assets to better RESONATE with more audiences, across more formats.</p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-solutions-part">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-3.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>ELATE<span class="purple-text">.</span></h2>
        <p>Discover digital advertising that can really RELATE to your customers using Real-Time Bidding and Programmatic Media Buying across all channels.</p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-solutions-part home-solutions-part-alt home-solutions-part-last">
    <div class="img-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/solution-h-4.jpg" alt=""></div>
    <div class="container">
      <div class="text">
        <h2><span class="purple-text">R</span>EJOICE<span class="purple-text">.</span></h2>
        <p>You can only truly REJOICE once a campaign delivers a HUGE return on investment.</p>
        <p>So warm up those vocal cords and give us an R… O… I!? </p>
        <a href="#" class="read-more">Read on</a>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.home-solutions-part -->
  <div class="home-testimonials">
    <div class="line-vertical-center line-vertical-center-top-over"></div>
    <div class="diagonal-line"></div>
    <div class="container">
      <div class="text">
        <h2>SOLVING <span>THE PUZZLE.</span></h2>
      </div>
      <!-- /.text -->
    </div>
    <!-- /.container -->
    <div class="testimonials-slider">
      <div class="slide">
        <div class="container">
          <div class="img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" class="pull-right" alt="">
          </div>
          <!-- /.img -->
          <div class="text">
            <h4>Client Name</h4>
            <p>Our job day in & day out is to help uncover exciting revelations about our client’s businesses, their audiences and where/how they should be better investing in media to boost business growth.</p>
            <a href="#" class="read-more">SEE THE STORY</a>
          </div>
          <!-- /.text -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.slide -->
      <div class="slide">
        <div class="container">
          <div class="img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" class="pull-right" alt="">
          </div>
          <!-- /.img -->
          <div class="text">
            <h4>Client Name</h4>
            <p>Our job day in & day out is to help uncover exciting revelations about our client’s businesses, their audiences and where/how they should be better investing in media to boost business growth.</p>
            <a href="#" class="read-more">SEE THE STORY</a>
          </div>
          <!-- /.text -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.slide -->
      <div class="slide">
        <div class="container">
          <div class="img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" class="pull-right" alt="">
          </div>
          <!-- /.img -->
          <div class="text">
            <h4>Client Name</h4>
            <p>Our job day in & day out is to help uncover exciting revelations about our client’s businesses, their audiences and where/how they should be better investing in media to boost business growth.</p>
            <a href="#" class="read-more">SEE THE STORY</a>
          </div>
          <!-- /.text -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.slide -->
    </div>
    <!-- /.testimonials-slider -->
  </div>
  <!-- /.home-testimonials -->
  <div class="get-in-touch">
  <div class="line-vertical-center line-vertical-center-top-alt"></div>
    <div class="container container-980">
      <div class="row row--80">
        <div class="col col--6 col--sm-12">
          <h2>GET IN <span>TOUCH</span></h2>
        </div>
        <!-- /.col col--6 -->
        <div class="col col--6 col--sm-12">
          <p>Does your Marketing need a Revelation? Pop us a call, leave a note or come visit us in person! </p>
          <a href="#" class="btn btn-white">SAY HELLO</a>
        </div>
        <!-- /.col col--6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.get-in-touch -->


</main>

<?php get_footer(); ?>
