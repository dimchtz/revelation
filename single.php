<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

<main role="main">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="news-single--thumbnail with-background-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>)">
    <div class="container container-small">
      <div class="heading">
        <?php $terms = get_the_terms(get_the_ID(), 'article_type'); if ( $terms ): ?>
          <a href="<?php echo esc_url(get_term_link($terms[0])); ?>" class="term" style="background-color: <?php echo get_field('color', $terms[0]); ?>;"><?php echo $terms[0]->name; ?></a>
        <?php endif; ?>
        <h1 class="font--tilt"><?php the_title(); ?></h1>
      </div>
    </div>
  </div>

  <div class="section section--normal-padding">
    <div class="container container-small">
      <div class="news-single--content">

        <div class="pre-content">
          <div class="meta">
            <span class="date"><?php the_time('d F Y, g:iA'); ?></span>
            <span class="comments"><a href="#the-comments" style="color:#eb1d2f"><i class="fa fa-comment"></i><?php echo get_comments_number(); ?> <?php _e('Comments', 'psdtheme'); ?></a></span>
          </div>
          <?php erl_print_share_links(); ?>
        </div>

        <div class="news-single--text text-area">
          <?php the_content(); ?>
        </div>

        <div class="post-content">
          <?php erl_print_share_links(); ?>
        </div>

        <?php get_template_part('template-parts/ads/detail'); ?>

        <div class="comments-wrapper" id="the-comments">
          <?php if ( get_comments_number() > 0 ): ?>
          <h3 class="custom-comments-title font--tilt"><span class="red">discussion</span> on “<?php the_title(); ?>”</h3>
          <?php else: ?>
          <h3 class="custom-comments-title font--tilt">Be the first to <span class="red">comment</span> on this article</h3>
          <?php endif; ?>
          <?php comments_template(); ?>
        </div>

      </div>
    </div>
  </div>

  <div class="section section--normal-padding">
    <div class="container">
      <div class="section-title section-title--center">
        <h2><?php _e('You may also be interested in...') ?></h2>
      </div>
      <div class="news-wrapper">
        <?php $post_id = get_the_ID();
        $cat_ids = array();
        $categories = get_the_category( $post_id );

        if(!empty($categories) && is_wp_error($categories)):
          foreach ($categories as $category):
            array_push($cat_ids, $category->term_id);
          endforeach;
        endif;

        $query = new WP_Query(array(
          'post_type'       => 'post',
          'posts_per_page'  => 3,
          'post__not_in'    => array( get_the_ID() ),
          'category__in'   => $cat_ids,
        )); if ( $query->have_posts() ): while ( $query->have_posts() ): $query->the_post(); ?>
        <div class="item-wrapper">
          <div class="item">
            <?php $terms = get_the_terms(get_the_ID(), 'article_type'); if ( $terms ): ?>
            <a href="<?php echo esc_url(get_term_link($terms[0])); ?>" class="term" style="background-color: <?php echo get_field('color', $terms[0]); ?>;"><?php echo $terms[0]->name; ?></a>
            <?php endif; ?>
            <a href="<?php echo esc_url(get_permalink()); ?>" class="thumbnail with-background-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>)"></a>
            <a href="<?php echo esc_url(get_permalink()); ?>" class="content">
              <h2 class="font--tilt"><?php the_title(); ?></h2>
              <div class="meta">
                <span class="date"><?php the_time('d F Y, g:iA'); ?></span>
                <span class="comments"><i class="fa fa-comment"></i><?php echo get_comments_number(); ?> <?php _e('Comments', 'psdtheme'); ?></span>
              </div>
              <p><?php echo wp_trim_words(get_the_excerpt(), 13); ?></p>
            </a>
          </div>
        </div>
        <?php endwhile; wp_reset_postdata(); endif; ?>
      </div><!-- ./news-wrapper -->
    </div>
  </div>

  <?php endwhile; endif; ?>

</main>

<?php get_footer(); ?>
