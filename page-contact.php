<?php
/**
 * Template Name: Contact
 */

get_header(); ?>

<main role="main" class="main-content">

  <div class="contact-hero">
    <div class="container container-980">
      <div class="row row--65">
        <div class="col col--7 col--sm-12">
          <h1>GET IN<span>TOUCH.</span></h1>
        </div>
        <!-- /.col col--7 col--sm-12 -->
        <div class="col col--5 col--sm-12">
          <p>Why not leave us a note, give us a call or pop into one of our offices for a visit.</p>
        </div>
        <!-- /.col col--5 col--sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container container-980 -->
  </div>
  <!-- /.contact-hero -->

  <div class="contact-main">
    <div class="container">
      <div class="would-like">
        <div class="row">
          <div class="col col--6 col--sm-12">
            <p>I WOULD LIKE TO</p>
          </div>
          <!-- /.col col--6 col--sm-12 -->
          <div class="col col--6 col--sm-12">
            <ul>
              <li><a href="" class= "jsPopupToggle" data-popup="#contact-popup">LEAVE MY DETAILS</a></li>
              <li class="active"><a href="">Request Info</a></li>
              <li><a href="">SUBMIT MY CV</a></li>
            </ul>
            <div class="line-vertical-left"></div>
          </div>
          <!-- /.col col--6 col--sm-12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.would-like -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.contact-main -->
  <div class="contact-where">
    <div class="diagonal-line"></div>
    <div class="container">
      <div class="row">
        <div class="col col--1">&nbsp;</div>
        <div class="col col--4 col--sm-12">
          <div class="contact-where-text jsContactCardMenu">
            <a href="#contact-card-1" class="jsContactCardTrigger block text-bebas text-bebas--big text-bebas--stroke active">Sydney</a>
            <a href="#contact-card-2" class="jsContactCardTrigger block text-bebas text-bebas--big text-bebas--stroke text--pink">UK</a>
            <a href="#contact-card-3" class="jsContactCardTrigger block text-bebas text-bebas--big text-bebas--stroke text--pink">UAE</a>
          </div>
        </div>
        <div class="col col--6 col--sm-12">
          <div class="contact-card">
            <div id="contact-card-1" class="contact-card-body active">
              <img src="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" alt="">
              <div class="contact-card-text">
                <p class="text-bebas text-bebas--big text-bebas--stroke contact-card-big-text">Sydney</p>
                <address>
                  <p>100 Harris St, Pyrmont NSW 2009</p>
                  <p>+61 402 030 520</p>
                </address>
              </div>
            </div>

            <div id="contact-card-2" class="contact-card-body">
              <img src="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" alt="">
              <div class="contact-card-text">
                <p class="text-bebas text-bebas--big text-bebas--stroke contact-card-big-text">Uk</p>
                <address>
                  <p>100 Harris St, Pyrmont NSW 2009</p>
                  <p>+61 402 030 520</p>
                </address>
              </div>
            </div>

            <div id="contact-card-3" class="contact-card-body">
              <img src="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" alt="">
              <div class="contact-card-text">
                <p class="text-bebas text-bebas--big text-bebas--stroke contact-card-big-text">Uae</p>
                <address>
                  <p>100 Harris St, Pyrmont NSW 2009</p>
                  <p>+61 402 030 520</p>
                </address>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php get_template_part('template-parts/misc/template' , 'find-more'); ?>
</main>

<div class="contact-popup-wrapper" id="contact-popup">
  <button class="close-popup jsPopupToggle" data-popup="#contact-popup">
    <svg xmlns="http://www.w3.org/2000/svg" width="20.41" height="19.895" viewBox="0 0 20.41 19.895">
      <g id="Group" transform="translate(10.253 -6.718) rotate(45)">
        <path id="Line_2" data-name="Line 2" d="M.5,0V24" transform="translate(11 0)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/>
        <path id="Line_2_Copy" data-name="Line 2 Copy" d="M0,.636H24" transform="translate(0 11)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/>
      </g>
    </svg>
  </button>
  <div class="popup-container">
    <a href="#" class="logo"><img src="<?= get_template_directory_uri() . '/images/logo.svg'; ?>" alt="<?= get_bloginfo('name'); ?>"></a>
    <h3 class="text-bebas text-bebas--big text-bebas--stroke leave-details-text">LEAVE MY DETAILS</h3>
    <div class="contact-form-wrapper">
      <form action="#" class="contact-form">
        <div class="form-group">
            <input type="text" name="firstname" placeholder="First Name">
        </div>
        <div class="form-group">
            <input type="text" name="lastname" placeholder="Last Name">
        </div>
        <div class="form-group">
            <input type="text" name="company" placeholder="Company">
        </div>
        <div class="form-group">
            <input type="email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            <input type="text" name="phone" placeholder="Phone">
        </div>
        <div class="form-group">
            <textarea name="message" id="" cols="30" rows="4" placeholder="Message"></textarea>
        </div>
        <input type="submit" class="form-submit" value="Send">
      </form>
    </div>
  </div>
</div>

<?php get_footer(); ?>
