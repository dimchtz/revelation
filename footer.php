<?php
/**
 * The template for displaying the footer.
 */
?>

  <footer>
    <div class="container">
      <div class="footer-top">
        <div class="row">
          <div class="col col--3 col--sm-12 hide--sm-up">
            <?php 
               $args = array(
                'theme_location' => 'primary',
                'container' => false, 
                'menu_class' => 'menu-lg'
              );
      
              wp_nav_menu($args); 
          ?>
          </div>
          <!-- /.col col--3 -->
          <div class="col col--4 col--sm-12">
            <?php 
               $args = array(
                'theme_location' => 'secondary',
                'container' => false, 
                'menu_class' => 'menu-sm'
              );
      
              wp_nav_menu($args); 
            ?>
            <?= get_field('footer_text', 'option'); ?>
          </div>
          <!-- /.col col--4 -->
          <div class="col col--3 col--sm-12 hide--sm-down">
              <?php 
                $args = array(
                  'theme_location' => 'primary',
                  'container' => false, 
                  'menu_class' => 'menu-lg'
                );
        
                wp_nav_menu($args); 
            ?>
          </div>
          <!-- /.col col--3 -->
          <div class="col col--3 col--sm-12 hide--sm-up">
            <div class="social-icons">
              <a href="#" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-twitter.svg" alt=""></a>
              <a href="#" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-linkedin.svg" alt=""></a>
              <a href="#" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-facebook.svg" alt=""></a>
            </div>
            <!-- /.social-icons -->
          </div>
          <div class="col col--5 col--sm-12">
            <?= wp_get_attachment_image(get_field('footer_logo','option')); ?>
          </div>
          <!-- /.col col--5 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.footer-top -->
      <div class="footer-bottom">
        <div class="row">
          <div class="col col--6 col--sm-12">
            <?= get_field('milkable_text','option'); ?>
          </div>
          <!-- /.col col--6 col--sm-12 -->
          <div class="col col--6 col--sm-12">
            <div class="social-icons hide--sm-down">
              <a href="<?= get_field('twitter_account_link','option'); ?>" target="_blank" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-twitter.svg" alt=""></a>
              <a href="<?= get_field('linkedin_link','option'); ?>" target="_blank" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-linkedin.svg" alt=""></a>
              <a href="<?= get_field('facebook_link','option'); ?>" target="_blank" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-facebook.svg" alt=""></a>
            </div>
            <!-- /.social-icons -->
          </div>
          <!-- /.col col--6 col--sm-12 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.footer-bottom -->
    </div>
    <!-- /.container -->
  </footer>

  <?php wp_footer(); ?>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>

</html>
