<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<main role="main">

  <div class="section section--normal-padding">
    <div class="container container-small">
      <div class="text-area">
        <?php if ( have_posts() ) { while ( have_posts() ) { the_post(); the_content(); } } ?>
      </div>
    </div>
  </div>

</main>

<?php get_footer(); ?>
