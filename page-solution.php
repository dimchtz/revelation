<?php

/**
 * Template Name: Solution Single
 */

get_header(); ?>

<main role="main" class="main-content">

  <div class="solutions__nav">
    <a href="#" class="is_active">Reveal.</a>
    <a href="#">Resonate.</a>
    <a href="#">Relate.</a>
    <a href="#">Rejoice.</a>
  </div><!-- ./solutions__nav -->

  <div class="hero-solution--single" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/sh1.png);">
    <div class="container">
      <div class="hero-text">
        <h1 class="text-bebas text-bebas--ultra-big text--pink">Reveal</h1>
        <h2>If you had all the answers today, how would it influence the decisions you make today?</h2>
        <h3>Because in today’s game, interpreting the data tomorrow can be too late!</h3>
      </div><!-- ./hero-text -->
    </div>
  </div><!-- ./hero-solution--single -->

  <div class="text-image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/ss1.png);">
    <div class="text">
      <h2>Smarter Data & Dashboards</h2>
      <p>Far too often we run into organisations using outdated reporting, or worse yet, excel files to run their company!</p>
      <p>Our Data & Dashboards aren’t just about visualising paid media performance, they’re focused on bringing key business insights out of the darkness and into the light, so that everyone we work with becomes a data-driven business!</p>
      <p>They help Reveal (in real-time), the game changing secrets which enable clients to climb that steep mountain of success by making the unknown, known! It’s about empowering all key decision makers with smarter data to execute more informed daily decisions with greater speed & accuracy than ever before.</p>
      <p>What could smarter data do to your bottom line?</p>
      <a href="#">See Example</a>
    </div><!-- ./text -->
  </div><!-- ./text-image -->

  <div class="text-image text-image--alt" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/ss2.png);">
    <div class="text">
      <h2>Metrics with Meaning</h2>
      <p>In today's fast-moving world of digital media and advertising, tracking every user interaction and touchpoint is paramount! But media metrics alone can become somewhat meaningless, unless they tell the full story about how a customer chooses to interact with your brand at every stage of their pre and post purchase journey (and the financial value gained versus the investment made)!</p>
      <p>To help make sense of the data and react swiftly to all those exciting engagements that are happening with real customers (or potential customers), your business data should be visualised and aligned alongside your media metrics, providing insights as they happen, in one place, for all your key teams to see, analyse, strategise & most importantly, take action to truly amplify your media investments!</p>
      <p>What next investment will generate you the biggest bang for buck?</p>
      <a href="#">See Example</a>
    </div><!-- ./text -->
  </div><!-- ./text-image -->

  <div class="text-image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/ss3.png);">
    <div class="text">
      <h2>A 360° Customer View</h2>
      <p>Imagine having every piece of Business Intelligence at your fingertips with a full 360 view of each new and existing customer to ensure every media dollar is directly accountable, justified and optimised to its fullest potential!</p>
      <p>By implementing our ad server and passing additional data at each point of conversion, we’re able to visulaise the entire path-to-conversion and analyse every single channel and message involved when influencing each purchasing decision, lead or enquiry with the greatest possible ROI.</p>
      <p>From prospecting & awareness messaging, to retargeting, nurturing, promotions and close offers across Banners, Videos, Social Ads/Posts, Keywords, Emails, Call Centre, Chats, CRM, Apps & more!</p>
      <p>And if one piece of the puzzle is not delivering the best possible ROI? Then you should know that too!</p>
      <a href="#">See Example</a>
    </div><!-- ./text -->
  </div><!-- ./text-image -->

</main>

<?php get_footer(); ?>
