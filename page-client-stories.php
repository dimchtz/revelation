<?php //Template Name: Client Stories

get_header();
?>
<main role="main" class="main-content">
<?php get_template_part('template-parts/client-stories/template' , 'client-stories-hero'); ?>
<?php get_template_part('template-parts/client-stories/template' , 'client-stories'); ?>
<div class="section-find-more">
    <div class="container">
        <a href="#" class="find-more-link">
            <h3 class="text-bebas">Find out more about our services </h3>
            <span class="arrow">
              <svg xmlns="http://www.w3.org/2000/svg" width="113.526" height="43.006" viewBox="0 0 113.526 43.006">
                <g id="Group_11" data-name="Group 11" transform="translate(0.594 0.503)">
                  <path id="Line_Copy_5" data-name="Line Copy 5" d="M110.625.875.406.813" transform="translate(0 20.056)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/>
                  <path id="Line_Copy_6" data-name="Line Copy 6" d="M28.51,1.076.406.813" transform="matrix(-0.704, 0.71, -0.71, -0.704, 112.367, 21.413)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/>
                  <path id="Line_Copy_7" data-name="Line Copy 7" d="M28.51-1.076.406-.812" transform="matrix(-0.704, -0.71, 0.71, -0.704, 112.367, 20.587)" fill="none" stroke="#fff" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2"/>
                </g>
              </svg>
            </span>
        </a>
    </div>
</div>

<div class="get-in-touch get-in-touch--client-stories no-triangle">
  <div class="line-vertical-center line-vertical-center-top-alt"></div>
  <div class="container container-980">
    <div class="row row--80">
      <div class="col col--6 col--sm-12">
        <h2>GET IN <span>TOUCH</span></h2>
      </div>
      <!-- /.col col--6 -->
      <div class="col col--6 col--sm-12">
        <p>Does your Marketing need a Revelation? Pop us a call, leave a note or come visit us in person! </p>
        <a href="#" class="btn btn-white">SAY HELLO</a>
      </div>
      <!-- /.col col--6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
<!-- /.get-in-touch -->
</main>

<?php
get_footer();