<?php
/**
 * The Header for our theme.
 */
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <!--<link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous" rel="stylesheet">-->
  <meta name="theme-color" content="#fafafa">

  <?php wp_head(); ?>

  <!--<script src="https://www.google.com/recaptcha/api.js?hl=en_US&onload=recaptchaCallback&render=explicit&ver=2.0"></script>-->

</head>

<body <?php body_class(); ?>>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <header class="header">
    <a href="#" class="header-toggle">menu toggle</a>
    <div class="header-content">
      <a href="<?php echo home_url('/'); ?>" class="logo">
        <img src="<?php echo get_field('logo', 'options')['url']; ?>" alt="<?php bloginfo('name'); ?>">
      </a>
    </div>
  </header>

  <div class="menu-wrapper">
    <div class="container">
      <?php 
        $args = array(
          'theme_location' => 'primary',
          'container' => false, 
          'menu_class' => 'menu'
        );

        wp_nav_menu($args); 
        ?>
      <!-- /.menu -->
      <div class="social-icons">
        <a href="<?= get_field('twitter_account_link','option'); ?>" target="_blank" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-twitter-white.svg" alt=""></a>
        <a href="<?= get_field('linkedin_link','option'); ?>" target="_blank" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-linkedin-white.svg" alt=""></a>
        <a href="<?= get_field('facebook_link','option'); ?>" target="_blank" class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-facebook-white.svg" alt=""></a>
      </div>
      <!-- /.social-icons -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.menu-wrapper -->
