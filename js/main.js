jQuery(document).ready(function($) {

  $('.testimonials-slider').slick({
    dots: true,
    speed: 300,
    slidesToShow: 1,
  });

  $('.header-toggle').on('click',function(e){
    e.preventDefault();

    $('body').toggleClass('body-menu-open');
    $('.menu-wrapper').fadeToggle();
  })

  var $jsContactCardTrigger = $('.jsContactCardTrigger');
  if( $jsContactCardTrigger.length > 0 ) { 

    $jsContactCardTrigger.on('click' , function(e) {
      e.preventDefault();
      var $this = $(this),
          target = $this.attr('href');

      $('.jsContactCardMenu .active').removeClass('active');
      $this.toggleClass('active');
    
      $('.contact-card .active').removeClass('active');
      $(target).toggleClass('active');
    });
  }

  var $jsPopupToggle = $('.jsPopupToggle');
  if( $jsPopupToggle.length > 0 ) {
    $jsPopupToggle.on('click' , function(e) {
      e.preventDefault();
      var target = $(this).attr('data-popup');

      $(target).toggleClass('active');
    });
  }


  /**
   * Client Stories
   */
  var $clientStoriesWrapper = $('.jsClientStoriesWrapper');
  if( $clientStoriesWrapper.length > 0 ) {
    var $clientStoriesMenuItems = $clientStoriesWrapper.find('.jsClientStoriesMenu a');
    var $clientStoriesImage = $clientStoriesWrapper.find('.jsClientImage');

    $clientStoriesMenuItems.on('mouseenter', function() {
      var $this = $(this);
      var imgsrc = $this.attr('data-image');

      $clientStoriesWrapper.find('.active').removeClass('active');
      $this.addClass('active');

      $clientStoriesImage.attr('style' , 'background-image:url('+imgsrc+');');
    })
  }


});
