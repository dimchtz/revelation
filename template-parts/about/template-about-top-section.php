<section class="about-top-section">
    <div class="container">
        <div class="row row--00">
            <div class="col col--6 col--sm-12">
                <div class="top-section-text">
                    <h1 class="text-bebas text-bebas--big text-bebas--white text-bebas--no-letter-spacing">Imagine a digital version of <span class="text-bebas text-bebas--ultra-big text-bebas--stroke">CSI.</span></h1>
                </div>
            </div>
            <div class="col col--6 col--sm-12">
                <div class="top-section-paragraph">
                    <p class="text--pink">Campaign Scene Investigators.</p>
                    <p>Just like the real CSI solving a crime, we dig deeper into all available business, competitive, consumer, customer and media data to uncover the valuable secrets that resolve marketing challenges, better relate to audiences & help put poor performing campaigns behind bars!</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.about-top-section -->

<section class="about-top-intro">
    <div class="line-vertical-center line-vertical-center-top"></div>
    <div class="row row--00">
        <div class="col col--6 col--sm-12">
            <div class="smoke-wrapper"></div>
            <div class="pink-wrapper-text"></div>
            <h2 class="text-bebas text-bebas--ultra-big text-bebas--stroke text-bebas--stroke-white"><span>The</span> <span>csi</span> agency</h2>
        </div>
        <div class="col col--6 col--sm-12">
            <img class="about-top-intro-image" src="<?= get_template_directory_uri() . '/images/csi-agency-photo.jpg'; ?>" alt="">
        </div>
    </div>
</section>