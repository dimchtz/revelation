<div class="section-divided-text">
    <div class="container">
        <div class="divided-text-wrapper">
            <div class="row">
                <div class="col col--5 col--sm-12">
                    <h3 class="text-bebas text-bebas--big text--pink">Data lights the path, <span class="text-bebas--stroke">not a stab in the dark. </span></h3>
                </div>
                <div class="col col--6 col--sm-12">
                    <div class="content-area">
                        <p>Forensic evidence is an investigators source of truth and it’s the same for us with data! </p>
                        <p>Our unique Investigative Process enables us to dig deeper into the data to find the hidden gems that drive positive outcomes. As a core part of our Agency’s DNA, it is this process which has helped ingrain a culture of genuine care for client results and in turn cultivated a deep-seeded passion for campaign performance that goes well above and beyond what’s expected – we can’t rest until the case has been solved (or the campaign delivers in spades)! </p>
                        <p>By investigating every piece of information available to us and letting the data dictate our next move, we’re able to stand behind every recommendation we make with absolute confidence on how best to invest each media, creative or marketing dollar – paving the way forward with a strategy founded on proven facts, not fiction. </p>
                        <p>And if we can’t all agree on the best way forward? Then we dust off the old search spotlight and it’s back to investigating for more clues until the optimum path ahead is clearly illuminated!</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.divided-text-wrapper -->

        <div class="divided-text-wrapper">
            <div class="row">
                <div class="col col--5 col--sm-12">
                    <h3 class="text-bebas text-bebas--big text--pink">A glass box, <span class="text-bebas--stroke">not a black box. </span></h3>
                </div>
                <div class="col col--6 col--sm-12">
                    <div class="content-area">
                        <p>By providing a fully transparent approach to media buying, we’re able to shine a light on all things programmatic and digital. We take the time to educate clients on the ins & outs of digital advertising, so they feel empowered to ask the right questions and challenge us in a collaborative and uber-open working environment. If the people we work with can know some of what we know, see the same data we see and are working towards the same goals we are, then everyone is generating insights and improving campaigns/outcomes together!  </p>
                        <p>It’s the whole story laid out as it happens, just as if we were an internal digital marketing team or inhouse trading desk working within an organisation (and in some cases we are), not hiding everything we do in a black box where budgets go in and nothing much else comes out. Imagine having a department in your business which helps shape, predict and guide you to future business success? </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.divided-text-wrapper -->

        <div class="divided-text-wrapper">
            <div class="row">
                <div class="col col--5 col--sm-12">
                    <h3 class="text-bebas text-bebas--big text--pink">An ADD Agency, <span class="text-bebas--stroke">not an Ad Agency.  </span></h3>
                </div>
                <div class="col col--6 col--sm-12">
                    <div class="content-area">
                        <p>Our mission in life is to ADD massive value to the clients and campaigns we are privileged work with.</p>
                        <p>High CTRs are nice but aligning advertising to business outcomes is what really excites us! That’s why our operational processes are 100% client-focussed and client-aligned so that we’re reporting on client metrics and speaking their KPIs, not our own.  </p>
                        <p>But the real added value begins when a campaign goes live! Only then can we start to unlock new insights and opportunities that deliver more meaningful audience experiences and truly drive business results. Our talented team of media traders optimise campaigns all day every day, while our client success managers are endlessly at work Knowing our client’s business/goals/challenges, Presenting our recommended solutions, Monitoring progress against those objectives, Connecting with key business stakeholders about them often and ensuring we are Delivering those outcomes within the organisation.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.divided-text-wrapper -->
        
    </div>
    <!-- /.container -->
</div>
<!-- /.section-divided-text -->