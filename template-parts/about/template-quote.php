<section class="section-quote">
    <div class="skyscraper"></div>
    <div class="line-vertical-center line-vertical-center-top"></div>
    <div class="container">
        <p class="text-bebas text-bebas--big text-bebas--white">A Skyscraper isn’t built from ground level. First you must dig deep, real deep!</p>
        <p class="text-bebas text-bebas--big text-bebas--white">W<span class="text--pink">e use this same methodology for every brand we help build.</span></p>
        <p class="text-bebas text-bebas--big text-bebas--white"><span class="text--pink">B</span>y digging deeper, valuable secrets can be uncovered.</p>
    </div>
</section>