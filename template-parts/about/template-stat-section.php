<section class="about-stat-section">
    <div class="line-vertical-center line-vertical-center-top"></div>
    <div class="container">
        <div class="about-stat-section-title">
            <h2 class="text-bebas text-bebas--big text-bebas--white"><span class="block"><span class="text-bebas--stroke text-bebas--stroke-white">A few</span> key</span> stats<span class="text-bebas--stroke text-bebas--stroke-white">:</span> </h2>
        </div>
    </div>

    <div class="stat-items-wrapper">
        <div class="stat-item">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">3</span>
            <span class="label text-bebas text-bebas--big text-bebas--white">Years RUNNING. </span>
        </div>
        <div class="stat-item">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">300%</span>
            <span class="label text-bebas text-bebas--big text-bebas--white">Growth YOY.</span>
        </div>
        <div class="stat-item">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">30</span>
            <span class="label text-bebas text-bebas--big text-bebas--white">PLUS CLIENTS.</span>
        </div>
        <div class="stat-item">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">3</span>
            <span class="label text-bebas text-bebas--big text-bebas--white">Global Offices. </span>
            <a href="#" class="text--pink dig-link">Want to Dig Deeper?</a>
        </div>
    </div>
</section>
<!-- /.about-stat-section -->