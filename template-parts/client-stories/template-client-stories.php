<section class="section-client-stories">
    <div class="diagonal-line"></div>
    <div class="container container-1210">
        <div class="client-stories-wrapper jsClientStoriesWrapper">
            <ul class="clients-menu jsClientStoriesMenu">
                <li><a href="#" data-image="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" class="text-bebas text-bebas--big text-bebas--stroke">Doors Plus</a></li>
                <li><a href="#" data-image="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" class="text-bebas text-bebas--big text-bebas--stroke">Hell Let Loose </a></li>
                <li><a href="#" data-image="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" class="text-bebas text-bebas--big text-bebas--stroke">ECCO Shoes </a></li>
                <li><a href="#" data-image="<?= get_template_directory_uri() . '/images/ubicar.jpg'; ?>"        class="text-bebas text-bebas--big text-bebas--stroke active">UBICAR</a></li>
                <li><a href="#" data-image="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" class="text-bebas text-bebas--big text-bebas--stroke">FOXTEL</a></li>
                <li><a href="#" data-image="<?= get_template_directory_uri() . '/images/contact-image.jpg'; ?>" class="text-bebas text-bebas--big text-bebas--stroke">Pantera Press </a></li>
            </ul>
            <div class="client-image jsClientImage" style="background-image: url(<?= get_template_directory_uri() . '/images/ubicar.jpg'; ?>)">

            </div>
        </div>
    </div>
</section>

