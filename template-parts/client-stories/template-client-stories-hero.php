<section class="client-stories-hero-section" style="background-image: url(<?= get_template_directory_uri() . '/images/client-stories-hero.jpg'; ?>)">
    <div class="container">
        <div class="client-stories-hero-text">
            <h1 class="text-bebas text-bebas--big text--pink text-bebas--no-letter-spacing">
                <span class="text-bebas--white">Talk is cheap.</span>
                <span class="text-bebas--white">U</span>nless it’s “Show me the money”!
            </h1>
        </div>
    </div>
    <div class="line-vertical-center line-vertical-center-bottom"></div>
</section>