<a href="<?php echo esc_url(get_permalink()); ?>" class="post-item-full-image video-single-post">
    <!--<div class="image-wrapper with-background-image has-overlay" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>)"></div>-->
    <div class="image-wrapper with-background-image has-overlay" style="background-image: url(https://img.youtube.com/vi/<?php echo get_field('video'); ?>/hqdefault.jpg)"></div>
    <div class="post-item-content">
        <div class="icon">
        </div>
        <div class="text">
            <h3 class="font--tilt"><?php the_title(); ?></h3>
            <span class="date text--date"><?php the_time('d F Y, g:iA'); ?></span>
            <span class="comments text--date"><i class="fa fa-comment"></i><?php echo get_comments_number(); ?> <?php _e('Comments', 'psdtheme'); ?></span>
        </div>
    </div>
    <!-- /.post-item-text -->
</a>
