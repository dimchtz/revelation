<section class="case-study-client-quote">
    <div class="container">
        <span class="quote-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="229" height="187" viewBox="0 0 229 187">
            <path id="_" data-name="“" d="M227,185H131.516V99.685L174.754,0H215.29L189.167,89.806H227V185ZM95.484,185H0V99.685L43.238,0H83.774L57.65,89.806H95.484V185Z" transform="translate(1 1)" fill="none" stroke="#b019d4" stroke-miterlimit="10" stroke-width="2"/>
            </svg>
        </span>
        <div class="quote">
            <p class="quote-title text-bebas text-bebas--big text-bebas--stroke">THE CLIENT</p>
            <blockquote class="content-area">
                <p>RD has been an amazing business partner for us to work with! We ran into a few tracking issues due to technology limitations and in-app tracking early on, but the guys persevered to ensure we were tracking everything and more! They wouldn’t even spend $1 of the budget until they were 100% certain the tracking was correctly setup (despite us wanting them to get some campaigns going)! </p>
                <p>It really shows how much they care about our business and our results, and how much they genuinely want to help influence success beyond just delivering a good campaign. We are thrilled with the way the partnership is going and excited to grow UbiCar into global insurance player – with the help of the team at RD of course 😊</p>
                <cite>CEO, UbiCar Australia</cite>
            </blockquote>
        </div>
        <div class="btn-wrapper text--center">
            <a href="#" class="btn btn-white">VIEW MORE</a>
        </div>
    </div>
</section>