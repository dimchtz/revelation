<section class="case-study-hero-section" style="background-image: url(<?= get_template_directory_uri() . '/images/case-study-hero.jpg'; ?>)">
    <div class="container">
        <div class="case-study-hero-text">
            <h1 class="text-bebas text-bebas--ultra-big text-bebas--stroke text-bebas--stroke-white">UbiCar</h1>
            <p class="text-bebas text-bebas--big text-bebas--white text-bebas--no-letter-spacing">A data-driven launch for a data-driven Car Insurance company.</p>
        </div>
    </div>
    <!-- /.container -->
</section>
<!-- /.case-study-hero-section -->

<div class="case-study-intro-section">
    <div class="diagonal-line"></div>
    <div class="line-vertical-center line-vertical-center-top"></div>
    <div class="container">
        
    </div>
</div>