<section class="case-study-stat-section">
    <div class="container">
        <div class="case-study-stat-section-title">
            <h2 class="text-bebas text-bebas--big text-bebas--white text-bebas--no-letter-spacing"><span class="text-bebas--stroke text-bebas--stroke-white">The</span>  results</h2>
        </div>
    </div>

    <div class="stat-items-wrapper stat-items-wrapper--case-study">
        <div class="stat-item stat-item--style-1">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">500%</span>
            <span class="label">Our average Quote to Policy duration has decreased by 500% and we are sitting at an ROAS of 22:1</span>
        </div>
        <div class="stat-item stat-item--style-2">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">GROWTH</span>
            <span class="label">UbiCar is still young with ambitious growth targets... but we haven’t missed one yet & are still scaling spend to bring even more category buyers into the funnel with no ceiling in sight! </span>
        </div>
        <div class="stat-item stat-item--style-1">
            <span class="number text-bebas text-bebas--ultra-big text-bebas--stroke">2020</span>
            <span class="label">With new products launching in 2020, we’re UBI-excited for what the future holds!</span>
        </div>
    </div>
</section>
<!-- /.about-stat-section -->