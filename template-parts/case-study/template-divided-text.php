<div class="section-divided-text section-divided-text--case-study">
    <div class="container">
        <div class="divided-text-group">
            <h2 class="divided-text-title text-bebas text-bebas--big text-bebas--stroke">The Revelations</h2>
            <div class="divided-text-wrapper">
                <div class="row">
                    <div class="col col--5 col--sm-12">
                        <h3 class="text-bebas text-bebas--big text--pink">Category:</h3>
                    </div>
                    <div class="col col--6 col--sm-12">
                        <div class="content-area">
                            <p>Car Insurance is one of the most competitive markets in the country! Luckily for UbiCar, many players offer similar products without much variation – a ripe market for disruption!  </p>
                            <p>With an average of <a href="#"> 77% (3 out of 4 Aussies!) choosing to auto-renew their car insurance</a> without getting another quote first, we knew users weren’t shopping around anymore, so we had to take our better offer to them! We also knew that the UbiCar business was still in startup mode and needed to put runs on the board early to complete, so going after existing policy holders at competitor brands which had high ‘switch potential’ was also leveraged. AAMI, Youi and Budget Direct were our top 3 according to our investigations. </p>
                            <p>Source: Roy Morgan General Insurance Currency Report 2018</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.divided-text-wrapper -->

            <div class="divided-text-wrapper">
                <div class="row">
                    <div class="col col--5 col--sm-12">
                        <h3 class="text-bebas text-bebas--big text--pink">Consumer: </h3>
                    </div>
                    <div class="col col--6 col--sm-12">
                        <div class="content-area">
                            <p>People are sick of paying higher premiums for other driver’s mistakes. Car Insurance providers drive up prices on car models which are involved in more accidents. </p>
                            <p>Like the Mazda 3 for example, there were over 8,000 of them sold in 2019 alone! With so many on the road, they can often be found in more accidents and therefor have higher insurance costs for anyone who drives one! Or young drivers... If one is bad, then they’re all bad? </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.divided-text-wrapper -->
        </div>
        <!-- /.divided-text-group -->

        <div class="divided-text-group">
            <h2 class="divided-text-title text-bebas text-bebas--big text-bebas--stroke">The work</h2>
            <div class="divided-text-wrapper">
                <div class="row">
                    <div class="col col--5 col--sm-12">
                        <h3 class="text-bebas text-bebas--big text--pink">Resonate:</h3>
                    </div>
                    <div class="col col--6 col--sm-12">
                        <div class="content-area">
                            <p>Ad creative was targeted to drive home our consumer insight “Why pay more for other bad drivers? Switch to cheaper/fairer car insurance with UbiCar”. For new cars “Save heaps on Cheaper Car Insurance, so you can buy a better Car! Multiple copy and image variations were created to A/B test and optimise towards best performers. </p>
                            <p>Dynamic Ads were also integrated to re-engage users based on who they were and how much someone similar to them save with UbiCar “Jane (also a female in her 20’s like you) saved over $300!” - the type of car they drove “showcasing images of their make/model and the live/actual low monthly price to insure their car, providing a discount for outstanding or aged quotes and then cross-selling and upselling policy holders to take out a second car or take advantage of our UbiCar’s family discount. Countdown timer ads are used to create urgency – counting down live to each users Policy Start Date. </p>
                            <a href="#" class="case-study-link">SEE EXAMPLE</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.divided-text-wrapper -->

            <div class="divided-text-wrapper">
                <div class="row">
                    <div class="col col--5 col--sm-12">
                        <h3 class="text-bebas text-bebas--big text--pink">Reveal:</h3>
                    </div>
                    <div class="col col--6 col--sm-12">
                        <div class="content-area">
                            <p>UbiCar monitors how you drive so they can offer far more competitive insurance that’s better tailored to you. Good drivers get rewarded. Infrequent drivers can get a cheaper deal. So naturally tracking potential drivers as they start a quote, complete a quote and finally purchase a policy (across desktop, mobile and app) is crucial! </p>
                            <p>Our dashboards merge all media data with quote and policy data to track actuals against growth targets, highlighting ROI across each Channel/Car Type/Car Manufacturer/Age Groups/Genders and varied policy value targets (higher premiums are better for business). This gives us huge amounts of data to influence the media buy each day. We then set out to add-in trip data, as UbiCar ranks and scores each driving trip based on speed, braking, turning and phone distractions. By linking driver data back to our media efforts, we further ensured our deliverables were not just about converting policy holders but focusing on attracting lots and lots of good drivers, who as it turns out, are much better customers for an insurance business! </p>
                            <a href="#">UbiCar Dashboard.????</a>
                            <a href="#" class="case-study-link">SEE EXAMPLE</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.divided-text-wrapper -->

            <div class="divided-text-wrapper">
                <div class="row">
                    <div class="col col--5 col--sm-12">
                        <h3 class="text-bebas text-bebas--big text--pink">Relate:</h3>
                    </div>
                    <div class="col col--6 col--sm-12">
                        <div class="content-area">
                            <p>By analysing time of day and day of week data for quotes and policies, we were quickly able to identify key time trends for when people were more likely to research, compare, quote and buy. People also prefer taking out a policy on desktop over a mobile device.</p>
                            <p>We aligned to these behaviors as part of our Always On prospecting & retargeting media strategy, upweighting and down weighting bid strategies to limit wastage and to better capture demand during peak times when interest is piqued, within key channels and environments. Search and Premium SEO are also utilized to own certain key terms we want to play in. These are debated and agreed on each month to where we see success.</p>
                            <a href="#">CarSales graphic (channels used? Display/Retargeting/Dynamic, Mobile/Near, Social, Native, Video, SEM, SEO, Affiliates, Marketing Automation)?????</a>
                            <a href="#" class="case-study-link">SEE EXAMPLE</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.divided-text-wrapper -->

            <div class="divided-text-wrapper">
                <div class="row">
                    <div class="col col--5 col--sm-12">
                        <h3 class="text-bebas text-bebas--big text--pink">Rejoice:</h3>
                    </div>
                    <div class="col col--6 col--sm-12">
                        <div class="content-area">
                            <p>By capturing a unique Quote & Policy ID, every media dollar can be tied back to what it generated as a business outcome. We analyse attribution models regularly to provide deeper insights into the top converting paths to conversion. </p>
                            <p>Creatives are tested and benchmarked against others in their feature set using stat sigs. If one falls below the threshold it is paused or re-worked. </p>
                            <a href="#">Live stat sig card? Or Goal tracking card??????</a>
                            <a href="#" class="case-study-link">SEE EXAMPLE</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.divided-text-wrapper -->
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.section-divided-text -->